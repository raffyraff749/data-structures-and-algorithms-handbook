# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Game Developer

## Rafael Ramos

👋 Aspiring Game Developer! 🚀👨‍💻 — 💌 rafaelramos@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_ramos_rafael.jpg](images/act_2_ramos_rafael.jpg)

### Bio

**Good to know:** I'm a fan of twice, i'm wife of Son Chaeyoung

**Motto:** Mottolog lang ang motto

**Languages:** Python, Java

**Other Technologies:** 

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->
